def zip_f(*iterable, strict):   #using the keyword argument 'strict' /True or False/
    if strict:  
        lengths = {len(ls) for ls in iterable}
        if len(lengths)==1:
            
            res=[]
            for i in range(len(iterable[0])):
                ls=[]
                for j in range (len(iterable)):
                    ls.append((iterable[j][i]))
                res.append(ls)  
            return tuple(res)
        return "You should note strict=False"  
    if not strict:
        min_length = min(len(ls) for ls in iterable)
        res=[]
        for i in range(min_length):
            ls=[]
            for j in range (len(iterable)):
                ls.append((iterable[j][i]))
            res.append(ls)  
        return tuple(res)  
        
ls1=[1,2] 
ls2=[3,4]
ls3=[6,7,9]
print(zip_f(ls1,ls2,ls3,strict=True))




def zip_f(*iterable):    # strict=False
    min_length = min(len(ls) for ls in iterable)
    res=[]
    
    for i in range(min_length):
        ls=[]
        for j in range (len(iterable)):
            ls.append((iterable[j][i]))
        res.append(ls)  
    return tuple(res)  
ls1=[1,2] 
ls2=[3,4,5]
ls3=[6,7,9]
print(zip_f(ls1,ls2,ls3))

def zip_f(*iterable):   # strict=True
    res=[]
    for i in range(len(iterable[0])):
        ls=[]
        for j in range (len(iterable)):
            ls.append((iterable[j][i]))
        res.append(ls)  
    return tuple(res)  
ls1=[1,2,3] 
ls2=[3,4,5]
ls3=[6,7,9]
print(zip_f(ls1,ls2,ls3))
